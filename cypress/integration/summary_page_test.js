describe("Summary Page test", () => {
  const username = Cypress.env("username");
  const password = Cypress.env("password");

  /*************************************** Log in ***************************************/
  it("Log in to platform", () => {
    // Visit demo
    cy.visit("/");

    // Input email
    cy.get("#user-Email").type(username);

    // Input password
    cy.get("#user-Password").type(password);

    // Click sign in
    cy.get("#user-form .signin-btn").click();
  });

  /*************************************** Show more functionality ***************************************/
  it("Check show more button(s) functionality", () => {
    // click show all button pinned if exists
    if (Cypress.$(".show-more-btn").length > 0) {
      cy.get(".show-more-btn").click();
    }

    // click show all button problem if exists
    if (Cypress.$(".show-more-btn-2").length > 0) {
      cy.get(".show-more-btn-2").click();
    }
  });

  /*************************************** Modal open functionality ***************************************/
  it("Check if problem areas and pinned controls open modal", () => {
    // check if problem/pinned exist
    if (Cypress.$(".border-prob-card").length > 0) {
      // loop through all problem pinned and click
      cy.get(".border-prob-card").each((item) => {
        cy.wrap(item).click({ force: true });
        // wait half seconds
        cy.wait(500);
        cy.get("[data-testid='main-modal-close']").click({ force: true });
      });
    }
  });

  /*************************************** Print functionality ***************************************/
  it("Check If print works", () => {
    // check print button
    cy.get(".row > button > .fa-print").should("exist");
  });
});
