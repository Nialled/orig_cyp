describe("UI test", () => {
  const username = Cypress.env("username");
  const password = Cypress.env("password");

  /*************************************** Log in ***************************************/
  it("Log in to platform", () => {
    // Visit demo
    cy.visit("/");

    // Input email
    cy.get("#user-Email").type(username);

    // Input password
    cy.get("#user-Password").type(password);

    // Click sign in
    cy.get("#user-form .signin-btn").click();
  });

  /*************************************** Sidebar Functionality ***************************************/
  it("Basic sidebar functionality", () => {
    // click button on sidebare to change view
    cy.get("#sidebarMinimize").click();

    // click request change link
    cy.get("[data-testid='request-change-link']").click();

    // click support link
    cy.get("[data-testid='support-link']").click();

    // test to see if widget showcase is present
    cy.get("[data-testid='widget-showcase-link']").should("not.exist");
  });

  /*************************************** Summary page Functionality ***************************************/
  it("Basic summary page functionality", () => {
    // click show all button pinned if exists
    if (Cypress.$(".show-more-btn").length > 0) {
      cy.get(".show-more-btn").click();
    }

    // click show all button problem if exists
    if (Cypress.$(".show-more-btn-2").length > 0) {
      cy.get(".show-more-btn-2").click();
    }

    // check if problem/pinned exist
    if (Cypress.$("#problemcard1").length > 0) {
      // loop through all problem pinned and click
      cy.get("#problemcard1").click();
      cy.get("[data-testid='main-modal-close']").click();
    }

    // check print button
    cy.get(".row > button > .fa-print").should("exist");
  });

  /*************************************** Settings Functionality ***************************************/
  it("Basic settings functionality", () => {
    // show setting dropdown
    cy.get("[data-testid='user-link']").click();

    // click the settings page
    cy.get("[data-testid='settings-link']").click();

    // check pinned controls modal
    cy.get("[data-testid='edit-pinned']")
      .click()
      .then(() => {
        // wait half seconds
        cy.wait(500);
      });
    cy.get("[data-testid='pinned-modal-close']").click({ force: true });

    // edit summary controls
    cy.get("[data-testid='edit-summary-groups']")
      .click()
      .then(() => {
        // wait half seconds
        cy.wait(500);
      });
    cy.get("[data-testid='summary-modal-close']").click({ force: true });

    // click checkoxes
    cy.get("[data-testid='gauge-checkbox'] > p > input").check({ force: true });
    cy.get("[data-testid='help-checkbox'] > p > input").check({ force: true });

    // go back to summary
    cy.get("[data-testid='nav-summary-link']").click({ force: true });
    cy.wait(5000);

    // go back to settings
    cy.get("#sidebarMinimize").click();
    cy.get("[data-testid='user-link']").click();
    cy.get("[data-testid='settings-link']").click();
    cy.wait(5000);

    // unclick checkboxes
    cy.get("[data-testid='gauge-checkbox'] > p > input").uncheck({
      force: true,
    });
    cy.get("[data-testid='help-checkbox'] > p > input").uncheck({
      force: true,
    });
  });

  /*************************************** Explore page Functionality ***************************************/
  it("Basic Explore page(s) functionality", () => {
    // click button on sidebare to change view
    cy.get("#sidebarMinimize").click();

    // click all side bar dropdowns
    cy.get(
      ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
    ).each(() => {
      cy.get(
        ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
      ).click({ multiple: true, force: true });
    });

    // click control listing
    if (Cypress.$("[data-testid='control-listing-link']").length > 0) {
      cy.get("[data-testid='control-listing-link']").click();
    }

    // click all side bar dropdowns
    cy.get(
      ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
    ).each(() => {
      cy.get(
        ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
      ).click({ multiple: true, force: true });
    });

    // click control approvals
    if (Cypress.$("[data-testid='control-approvals-link']").length > 0) {
      cy.get("[data-testid='control-approvals-link']").click();
    }

    // click all side bar dropdowns
    cy.get(
      ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
    ).each(() => {
      cy.get(
        ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
      ).click({ multiple: true, force: true });
    });

    // click metrics and measures
    if (Cypress.$("[data-testid='control-metrics-link']").length > 0) {
      cy.get("[data-testid='control-metrics-link']").click();
    }

    // click all side bar dropdowns
    cy.get(
      ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
    ).each(() => {
      cy.get(
        ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
      ).click({ multiple: true, force: true });
    });

    // click risk accepted
    if (Cypress.$("[data-testid='control-risk-accepted-link']").length > 0) {
      cy.get("[data-testid='control-risk-accepted-link']").click();
    }
  });

  /*************************************** Navbar Functionality ***************************************/
  it("Basic navbar functionality", () => {
    // check if all buttons exist
    cy.get("[data-testid='nav-notif-dropdown-link']").should("exist");
    cy.get("[data-testid='nav-summary-link']").should("exist");
    cy.get("[data-testid='nav-signout-link']").should("exist");
    // click summary page
    cy.get("[data-testid='nav-summary-link']").click();
    // click notifications button in navbar
    cy.get("[data-testid='nav-notif-dropdown-link']").click();
    cy.get("[data-testid='nav-notif-dropdown-link']").click();
    // click log out button
    cy.get("[data-testid='nav-signout-link']").click();
  });
});
