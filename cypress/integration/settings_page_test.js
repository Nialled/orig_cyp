describe("Summary Page test", () => {
  const username = Cypress.env("username");
  const password = Cypress.env("password");

  /*************************************** Log in ***************************************/
  it("Log in to platform", () => {
    // Visit demo
    cy.visit("/");

    // Input email
    cy.get("#user-Email").type(username);

    // Input password
    cy.get("#user-Password").type(password);

    // Click sign in
    cy.get("#user-form .signin-btn").click();
  });

  /*************************************** Go to settings page ***************************************/
  it("Navigate to settings page", () => {
    // click button on sidebare to change view
    cy.get("#sidebarMinimize").click();

    // show setting dropdown
    cy.get("[data-testid='user-link']").click();

    // click the settings page
    cy.get("[data-testid='settings-link']").click();
  });

  /*************************************** Checkbox functionality ***************************************/
  it("Toggle gauge and help checkboxes", () => {
    // click gauge checkbox
    cy.get("[data-testid='gauge-checkbox'] > p > input").check({ force: true });

    // click help checkbox
    cy.get("[data-testid='help-checkbox'] > p > input").check({ force: true });

    // go back to summary
    cy.get("[data-testid='nav-summary-link']").click({ force: true });

    // check to see if there is number in summary
    cy.get("#totalControls").should("not.contain", /[1-9]*/);

    // go back to settings page and toggle checkboxes
    cy.get("#sidebarMinimize").click();
    cy.get("[data-testid='user-link']").click();
    cy.get("[data-testid='settings-link']").click();
    cy.wait(5000);
    cy.get("[data-testid='gauge-checkbox'] > p > input").uncheck({
      force: true,
    });
    cy.get("[data-testid='help-checkbox'] > p > input").uncheck({
      force: true,
    });

    // check if summary does contain number
    cy.get("[data-testid='nav-summary-link']").click();
    cy.get("#totalControls > a").contains(/[1-9]*/);

    // go back to settings page
    cy.get("#sidebarMinimize").click();
    cy.get("[data-testid='user-link']").click();
    cy.get("[data-testid='settings-link']").click();
  });

  /*************************************** Adding/removing pinned  ***************************************/
  it("Add and remove pinned control", () => {
    // edit pinned controls
    cy.get("[data-testid='edit-pinned']")
      .click()
      .then(() => {
        // wait half seconds
        cy.wait(500);
      });

    // add pinned control
    cy.get(".dataTables_scrollBody")
      .eq(1)
      .then(() => {
        if (
          Cypress.$(
            "#all-controls-table_wrapper > .row > .col-sm-12 > .dataTables_scroll > .dataTables_scrollBody > table > tbody > tr > td > .btn > i"
          ).length > 4
        ) {
          cy.get(
            "#all-controls-table_wrapper > .row > .col-sm-12 > .dataTables_scroll > .dataTables_scrollBody > table > tbody > tr > td > .btn > i"
          )
            .eq(5)
            .click();
        }
      });

    // close edit pinned modal
    cy.get("[data-testid='pinned-modal-close']").click({ force: true });
    cy.reload();
    cy.wait(5000);

    // Check if correct summary group has been added
    cy.get("#pcListNames > p")
      .eq(4)
      .then(($el) => {
        // check added pinned name
        const pinnedName = $el.text();

        // go back to summary
        cy.get("[data-testid='nav-summary-link']")
          .click({ force: true })
          .then(() => {
            cy.wait(1000);
          });

        // click show more buttons if there
        cy.get(".show-more-btn").click();

        // check if group has been added
        cy.get("#pinnedcard5 > h6").contains(pinnedName);
      });

    // go back to summary
    cy.get("[data-testid='nav-summary-link']")
      .click({ force: true })
      .then(() => {
        // wait 3 seconds to see if pinned has increased
        cy.wait(3000);
      });

    // go back to settings page and toggle checkboxes
    cy.get("#sidebarMinimize").click();
    cy.get("[data-testid='user-link']").click();
    cy.get("[data-testid='settings-link']").click();

    // edit pinned controls
    cy.get("[data-testid='edit-pinned']").click();

    // remove pinned control
    cy.get(".dataTables_scrollBody")
      .eq(1)
      .then(() => {
        if (
          Cypress.$(
            "#pinned-controls-table_wrapper > .row > .col-sm-12 > .dataTables_scroll > .dataTables_scrollBody > table > tbody > tr > td > .btn > i"
          ).length > 3
        ) {
          cy.get(
            "#pinned-controls-table_wrapper > .row > .col-sm-12 > .dataTables_scroll > .dataTables_scrollBody > table > tbody > tr > td > .btn > i"
          )
            .eq(4)
            .click();
          // wait 3 seconds to see if pinned has increased
          cy.wait(1000);
        }
      });

    // close edit pinned modal
    cy.get("[data-testid='pinned-modal-close']").click({ force: true });
  });

  /*************************************** Adding/removing summary  ***************************************/
  it("Add and remove summary group", () => {
    // edit summary controls
    cy.get("[data-testid='edit-summary-groups']")
      .click()
      .then(() => {
        // wait half seconds
        cy.wait(500);
      });

    // add summary groupl
    cy.get("#summary-groups-table_wrapper").then(() => {
      if (
        Cypress.$(
          "#all-groups-table_wrapper > .row > .col-sm-12 > .dataTables_scroll > .dataTables_scrollBody > table > tbody > tr > td > .btn > i"
        ).length > 0
      ) {
        cy.get(
          "#all-groups-table_wrapper > .row > .col-sm-12 > .dataTables_scroll > .dataTables_scrollBody > table > tbody > tr > td > .btn > i"
        )
          .eq(0)
          .click();
      }
    });

    // close edit summary modal
    cy.get("[data-testid='summary-modal-close']").click({ force: true });

    // Check if correct summary group has been added
    cy.get("#sgListNames > p")
      .eq(0)
      .then(($el) => {
        // check added group name
        const summaryName = $el.text();

        // go back to summary
        cy.get("[data-testid='nav-summary-link']")
          .click({ force: true })
          .then(() => {
            cy.wait(1000);
          });

        // check if group has been added
        cy.get(".summary-item > span").last().contains(summaryName);
      });

    // go back to settings page
    cy.get("#sidebarMinimize").click();
    cy.get("[data-testid='user-link']").click();
    cy.get("[data-testid='settings-link']").click();

    // edit summary controls
    cy.get("[data-testid='edit-summary-groups']").click();

    // remove summary group
    cy.get("#summary-groups-table_wrapper").then(() => {
      if (
        Cypress.$(
          "#summary-groups-table_wrapper > .row > .col-sm-12 > .dataTables_scroll > .dataTables_scrollBody > table > tbody > tr > td > .btn > i"
        ).length > 0
      ) {
        cy.get(
          "#summary-groups-table_wrapper > .row > .col-sm-12 > .dataTables_scroll > .dataTables_scrollBody > table > tbody > tr > td > .btn > i"
        )
          .eq(0)
          .click({ force: true });
        // wait 3 seconds to see if pinned has increased
        cy.wait(3000);
      }
    });

    // close edit summary modal
    cy.get("[data-testid='summary-modal-close']").click({ force: true });
  });
});
