describe("Group modal test", () => {
  const username = Cypress.env("username");
  const password = Cypress.env("password");

  /*************************************** Log in ***************************************/
  it("Log in to platform", () => {
    // Visit demo
    cy.visit("/");

    // Input email
    cy.get("#user-Email").type(username);

    // Input password
    cy.get("#user-Password").type(password);

    // Click sign in
    cy.get("#user-form .signin-btn").click();
  });

  /*************************************** Go to control listing page ***************************************/
  it("Navigate to control listing page", () => {
    // navigate to controls explorer page
    cy.get("#sidebarMinimize").click();
    cy.get("[href='#pagesExplore']").click();
    cy.get("[data-testid='control-listing-link']").click();
  });

  /*************************************** Test name of modal ***************************************/
  it("Test modal name", () => {
    // check name of first control and check it aligns with modal
    cy.get("#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td")
      .eq(1)
      .then(($el) => {
        const controlName = $el.text();
        $el.click();
        cy.wait(500);
        cy.get(".modal-header > .col-md-10 > #itemTitle").should(
          "have.text",
          controlName
        );
      });
  });

  /*************************************** Test score ***************************************/
  it("Test modal score", () => {
    // Set viewport
    cy.viewport(1450, 900);

    // close modal
    cy.get("[data-testid='main-modal-close']").click({ force: true });
    // check table score
    cy.get(
      "#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td > .badge"
    )
      .eq(0)
      .then(($el) => {
        const controlBadge = $el.text();
        const controlString = controlBadge.substr(0, controlBadge.length - 1);
        const controlScore = controlString.trim();

        // open modal again
        cy.get(
          "#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td"
        )
          .eq(1)
          .click();
        cy.wait(500);

        // check if scores match
        cy.get("#controlscoreindex").should("contain", controlScore);
      });
  });

  /*************************************** Test relationship panel ***************************************/
  it("Test relationship panel", () => {
    // Set viewport
    cy.viewport(1450, 900);

    // check if relationships panel exist
    cy.get("#relationshipsCard > .card-body > svg").should("be.visible");

    cy.get(".modal-header > .col-md-10 > #itemTitle").then(($el) => {
      const controlName = $el.text();

      // click node 1 level back and check name
      cy.get("g.node")
        .eq(1)
        .click()
        .then(() => {
          cy.wait(500);
        });

      // check name is not the same and go back
      cy.get(".modal-header > .col-md-10 > #itemTitle").should(
        "not.contain",
        controlName
      );
      cy.get("g.node").eq(2).click();

      // click node 2 levels back and check name is not the same
      cy.get("g.node")
        .eq(2)
        .click()
        .then(() => {
          cy.wait(500);
        });
      cy.get(".modal-header > .col-md-10 > #itemTitle").should(
        "not.contain",
        controlName
      );

      // go back to original control
      cy.get("g.node").eq(8).click();
      cy.get("g.node").eq(2).click();

      // click final node and check name is not the same
      cy.get("g.node")
        .eq(2)
        .click()
        .then(() => {
          cy.wait(500);
        });
      cy.get(".modal-header > .col-md-10 > #itemTitle").should(
        "not.contain",
        controlName
      );

      // go back to original control
      cy.get("g.node").eq(8).click();
      cy.get("g.node").eq(2).click();
    });
  });

  /*************************************** Test the description ***************************************/
  it("Test description", () => {
    // close the modal and get description
    cy.get("[data-testid='main-modal-close']").click({ force: true });
    cy.get("#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td")
      .eq(2)
      .then(($el) => {
        const rawTxt = $el.text();
        const txt = rawTxt.replace("...", "");
        const description = txt.trim();

        // open modal again
        cy.get(
          "#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td"
        )
          .eq(1)
          .click();
        cy.wait(500);

        // check if descriptions match
        cy.get("#smallitemdesc").should("contain", description);
      });

    // check if description expands
    cy.get("#smallitemdesc").then(($el) => {
      const txt = $el.text();
      cy.log(txt.length);

      if (txt.length >= 68) {
        // click description
        cy.get("#smallitemdesc").click();

        // full item description should be present
        cy.get("#fullitemdesc").should("exist");
      }
    });
  });

  /*************************************** Test the tags ***************************************/
  it("Test tags", () => {
    // close the modal and get 1st tag
    cy.get("[data-testid='main-modal-close']").click({ force: true });
    cy.get(
      "#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td > .btn-outline-qo-white-grey"
    )
      .eq(0)
      .then(($el) => {
        const rawTxt = $el.text();
        const txt = rawTxt.replace("control-monitored", "");

        // open modal again
        cy.get(
          "#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td"
        )
          .eq(1)
          .click();
        cy.wait(500);

        // check if first tage matches
        cy.get("#tags > .btn-outline-qo-white-grey").should("contain", txt);
      });

    // close modal and get second tag
    cy.get("[data-testid='main-modal-close']").click({ force: true });
    cy.get(
      "#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td > .btn-outline-qo-white-grey"
    )
      .eq(1)
      .then(($el) => {
        const rawTxt = $el.text();
        // const txt = rawTxt.replace("control-monitored", "");

        // open modal again
        cy.get(
          "#datatable_wrapper > .row > .col-sm-12 > table > tbody > tr > td"
        )
          .eq(1)
          .click();
        cy.wait(500);

        // check if first tage matches
        cy.get(".qodarkgreycolour").should("contain", rawTxt);
      });
  });

  /*************************************** Test metdata panel ***************************************/
  it("Test metadata", () => {
    // check last seen
    cy.get("#ls").should("exist");

    // check control type
    cy.get("#ct").should("exist");

    // check control owner
    cy.get("#co").should("exist");

    // check control approver
    cy.get("#ca").should("exist");

    // check target state
    cy.get("#target").should("exist");

    // check baseline state
    cy.get("#baseline").should("exist");

    // check Monitored
    cy.get("#monitored > .fas").should("exist");
  });

  /*************************************** Test trend graoh ***************************************/
  it("Test trend panel", () => {
    // check graph is present
    cy.get("#itemcountgraph").should("exist");

    // check 1 week btn
    cy.get("#now-1w-btn")
      .click()
      .then(() => {
        cy.wait(500);
        cy.get("#itemcountgraph").should("exist");
      });

    // check 1 month btn
    cy.get("#now-1M-btn")
      .click()
      .then(() => {
        cy.wait(500);
        cy.get("#itemcountgraph").should("exist");
      });

    // check 3 month btn
    cy.get("#now-3M-btn")
      .click()
      .then(() => {
        cy.wait(500);
        cy.get("#itemcountgraph").should("exist");
      });

    // check 6 month btn
    cy.get("#now-6M-btn")
      .click()
      .then(() => {
        cy.wait(500);
        cy.get("#itemcountgraph").should("exist");
      });

    // check 12 month btn
    cy.get("#now-12M-btn")
      .click()
      .then(() => {
        cy.wait(500);
        cy.get("#itemcountgraph").should("exist");
      });

    // go back to 24hr
    cy.get("#now-24h-btn").click();
    cy.get("#itemcountgraph").should("exist");
  });

  /*************************************** Test picker panel ***************************************/
  it("Test evidence picker panel", () => {
    // check evidence panel is present
    cy.get("#cal-heatmap > .cal-heatmap-container").should("exist");
    cy.get("#cal-heatmap-hours > .cal-heatmap-container").should("exist");

    // click on of the nodes
    cy.get(
      "#cal-heatmap > .cal-heatmap-container > .graph > .m_5 > .graph-subdomain-group > :nth-child(19) > .graph-rect"
    ).click();

    // click another
    cy.get(
      ".m_3 > .graph-subdomain-group > :nth-child(19) > .graph-rect"
    ).click();
  });

  /*************************************** Test table panel ****************************************/
  it("Test evidence table panel", () => {
    // check evdence table is present
    cy.get("#evidenceCard").should("exist");

    // check if subitems is present
    if (Cypress.$("#subcontcontent").length > 0) {
      cy.get("#subcontcontent").should("exist");
    }

    // if evidence exists check if time badge is present
    if (Cypress.$("#evidence-date .badge").length > 0) {
      cy.get("#evidence-date > .badge").should("exist");
    }
  });
});
