describe("All widget tests", () => {
  const username = Cypress.env("username");
  const password = Cypress.env("password");

  /*************************************** Log in ***************************************/
  it("Log in to platform", () => {
    // Visit demo
    cy.visit("/");

    // Input email
    cy.get("#user-Email").type(username);

    // Input password
    cy.get("#user-Password").type(password);

    // Click sign in
    cy.get("#user-form .signin-btn").click();
  });

  /*************************************** Summary group widget ***************************************/
  it("Test group summary widget", () => {
    // check score in card
    if (Cypress.$(".summary-score").length > 0) {
      cy.get(".summary-score").each((item) => {
        // check if score contains number
        cy.wrap(item).contains(/[1-9]*/);
      });
    }

    // check if colour class of score is correct
    cy.get(".summary-score")
      .eq(0)
      .then(($btn) => {
        const txt = $btn.text();
        const score = parseFloat(txt.substr(0, txt.indexOf("/")));
        if (score > 9 && score < 10) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-9a"
          );
        } else if (score > 8 && score < 9) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-8a"
          );
        } else if (score > 7 && score < 8) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-7a"
          );
        } else if (score > 6 && score < 7) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-6a"
          );
        } else if (score > 5 && score < 6) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-5a"
          );
        } else if (score > 4 && score < 5) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-4a"
          );
        } else if (score > 3 && score < 4) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-3a"
          );
        } else if (score > 2 && score < 3) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-2a"
          );
        } else if (score > 1 && score < 2) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-1a"
          );
        } else if (score > 0 && score < 1) {
          cy.get(".summary-score").should(
            "have.class",
            "qo-colour-perf-rating-0a"
          );
        }
      });

    // Wait for chart to load
    if (Cypress.$(".summary-chart-container-1").length > 0) {
      cy.get(".anychart-ui-support")
        .should("be.visible")
        .then(() => {
          // wait 1 second for chart animation
          cy.wait(1000);
        });
    }

    // check if line charts are visible
    if (Cypress.$(".summary-chart-container-1").length > 0) {
      cy.get(".summary-chart-container-1").each((item) => {
        cy.wrap(item)
          .should("be.visible")
          .and((chart) => {
            // Check chart height
            expect(chart.height()).to.be.greaterThan(50);
          });
      });
    }

    // check if bar charts are visible
    if (Cypress.$(".summary-chart-container-2").length > 0) {
      cy.get(".summary-chart-container-2").each((item) => {
        cy.wrap(item)
          .should("be.visible")
          .and((chart) => {
            // Check chart height
            expect(chart.height()).to.be.greaterThan(150);
          });
      });
    }

    // check tooltip for  line charts
    if (Cypress.$(".summary-chart-container-1").length > 0) {
      cy.get(".summary-chart-container-1").each((item) => {
        cy.wrap(item).trigger("mouseover");
      });
    }
  });

  /*************************************** Heatmap widget ***************************************/
  it("Test heatmap widget", () => {
    // Navigate to Nist dashboard
    cy.get("#sidebarMinimize").click();
    cy.get(
      ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
    ).each(() => {
      cy.get(
        ".sidebar > .sidebar-wrapper > ul > li > [data-toggle='collapse']"
      ).click({ multiple: true, force: true });
    });

    // Click Nist Dashboard
    if (Cypress.$("[href='/v2/dash/nist']").length > 0) {
      cy.get("[href='/v2/dash/nist']").click();
    }

    // Make sure heatmap is visible
    if (Cypress.$(".heatmapv2").length > 0) {
      cy.get(".heatmapv2")
        .should("be.visible")
        .then(() => {
          // wait 1 second for chart animation
          cy.wait(1000);
          // check tooltip works on hover
          cy.get(".heatmapv2 > .heatmapv2 > div > svg").trigger("mouseover");
        });
    }

    // click buttons and test if text changes
    cy.get("[href='#IDENTIFY']").then(($btn) => {
      const txt = $btn.text();
      $btn.click();
      cy.get(
        ".row > .col-lg-6 > .row > .col-md-8 > .tab-content > .tab-pane > p"
      ).contains(txt);
    });
    cy.get("[href='#PROTECT']").then(($btn) => {
      const txt = $btn.text();
      $btn.click();
      cy.get(
        ".row > .col-lg-6 > .row > .col-md-8 > .tab-content > .tab-pane > p"
      ).contains(txt);
    });
    cy.get("[href='#DETECT']").then(($btn) => {
      const txt = $btn.text();
      $btn.click();
      cy.get(
        ".row > .col-lg-6 > .row > .col-md-8 > .tab-content > .tab-pane > p"
      ).contains(txt);
    });
    cy.get("[href='#RESPOND']").then(($btn) => {
      const txt = $btn.text();
      $btn.click();
      cy.get(
        ".row > .col-lg-6 > .row > .col-md-8 > .tab-content > .tab-pane > p"
      ).contains(txt);
    });
    cy.get("[href='#RECOVER']").then(($btn) => {
      const txt = $btn.text();
      $btn.click();
      cy.get(
        ".row > .col-lg-6 > .row > .col-md-8 > .tab-content > .tab-pane > p"
      ).contains(txt);
    });
  });

  /*************************************** Radar widget ***************************************/
  it("Test radar widget", () => {
    // check to see if radar chart is visible
    if (Cypress.$("#riskradar").length > 0) {
      cy.get("#riskradar")
        .should("be.visible")
        .and((chart) => {
          // Check chart height
          expect(chart.height()).to.be.greaterThan(190);
        });
    }
  });
});
